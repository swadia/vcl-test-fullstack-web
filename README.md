# VclTestFullstackWeb

## Objectives of the test

Adapt this program to display a grid of blinking colored squares.
At the begining, no square blinks, then each second, one new square should blink et this until every square of the grid blinks. The new blinking square must be selected randomly. Each square must blink indenpendantly from each other.

The color choice of the blinking is left to the candidate.

Try with only 4 square to start then raise it to 64.

Each square of one row must be colored with gradient symetrical colors 
Each square of one line must be colored with gradient symetrical colors.

The squares of the grid stop blinking when all squares of the grid have blinked.



## Dependencies
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.6.
This project depends on material component already loaded into this angular app : https://material.angular.io/components/grid-list/overview

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

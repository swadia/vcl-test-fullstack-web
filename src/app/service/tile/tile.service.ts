import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Tile } from '../../models/tile.model';
import { Observable, of, from } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TileService {

  // some colors
  private baseColors = ['red', 'azure', 'yellow', 'green', 'blue', 'purple', 'orange', 'cyan'];
  private tileArray: Tile[] = [];

  // service to return  a table tiles
  public initTileForExample(): Observable<Tile[]> {
    for (let i=0;i<=63; i++){
      let color = this.baseColors[Math.floor (i / 8)];
        this.tileArray.push(new Tile(1, 1, i.toString(), 'linear-gradient(' + color + ', grey)' ));
    }
    return of(this.tileArray);
  }
}

export class Tile {
    color: string;
    cols: number;
    rows: number;
    text: string;
    constructor(cols: number, rows: number, text: string, color: string) {
      this.color = color;
      this.text = text;
      this.rows = rows;
      this.cols = cols;
    }
}

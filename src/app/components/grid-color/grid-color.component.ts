import { Component, OnInit } from '@angular/core';
import { TileService } from '../../service/tile/tile.service';
import { Tile } from '../../models/tile.model';
import {Observable, of, from} from "rxjs/index";
import {delay, concatMap} from "rxjs/internal/operators";

@Component({
  selector: 'app-grid-color',
  templateUrl: './grid-color.component.html',
  styleUrls: ['./grid-color.component.scss']
})
export class GridColorComponent implements OnInit  {

  // number of columns on the grids
  columns = 8;
  tileHeight = '100px';
  // number of squares in grid
  limit : number = 63;
  // table contain index of squares
  unique_random_indexs : Array<number> = [];

  tiles: Observable<Tile[]>;

  blinkingIndex: Observable<number>;

  constructor(public tileService: TileService) {
    //fill the table with random different index
    while (this.unique_random_indexs.length < this.limit) {
      let newNumber = Math.floor (Math.random() * this.limit);
        if (this.unique_random_indexs.indexOf(newNumber) === -1) {
          this.unique_random_indexs.push(newNumber);
        }
    }
  }

  ngOnInit() {
    // initialize the tiles observable
    this.tiles = this.tileService.initTileForExample();
    // initiliaze the index observable
    this.blinkingIndex = from(this.unique_random_indexs ).pipe(
      concatMap( item => of(item).pipe ( delay( 1000 )))
    );

    this.blinkingIndex.subscribe((index: number) => console.log('this is the index to blink : ', index));
  }
}
